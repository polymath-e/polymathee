import ModeratorPAge from '../moderatorPage/ModeratorPage';

const AdminPage = () => {

    return(
        <ModeratorPAge role={'Administrator'}/>
    );
}

export default AdminPage;