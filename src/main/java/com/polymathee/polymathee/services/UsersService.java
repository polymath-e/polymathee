package com.polymathee.polymathee.services;

import com.polymathee.polymathee.dao.User;

import java.util.List;

public interface UsersService {
    List<User> getUserList();
    User getUserByID(Integer id);
    void DeleteUserById(Integer id);
    User FindUserByEmail(String email);
}
