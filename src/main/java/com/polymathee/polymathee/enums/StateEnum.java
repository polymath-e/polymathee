package com.polymathee.polymathee.enums;

public enum StateEnum {
    Saved, To_Treat, Published, Rejected
}
