package com.polymathee.polymathee.enums;

public enum RoleEnum {
    Student, Moderator, Administrator
}
