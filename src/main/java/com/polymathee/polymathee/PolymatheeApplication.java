package com.polymathee.polymathee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolymatheeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PolymatheeApplication.class, args);
	}

}
