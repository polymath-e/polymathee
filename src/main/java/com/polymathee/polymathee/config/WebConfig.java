package com.polymathee.polymathee.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.beans.factory.annotation.Value;



@Configuration
public class WebConfig implements WebMvcConfigurer {
    
    @Value("${cors.origin.url}")
    private String corsOriginUrl;
    
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
        .allowedOrigins(corsOriginUrl)
        .allowedMethods("GET","POST","PUT","DELETE");
    }
}